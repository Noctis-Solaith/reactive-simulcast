import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AnimeCollection } from './anime';

@Injectable({
  providedIn: 'root'
})
export class AnimeService {
  private root_url = `http://localhost:5000/api/v1/animes`;

  constructor(private http: HttpClient) {}

  getAnimes(year?: number, season?: string): Observable<AnimeCollection> {
    let request = this.root_url;
    const today = new Date();
    const selectedYear = year || today.getFullYear();
    const selectedSeason = season || this.getCurrentSeason(today.getMonth() + 1);
    if (year && season) {
      request = `${
        this.root_url
      }?year=${selectedYear}&season=${selectedSeason}`;
    }

    return this.http.get<AnimeCollection>(request);
  }

  private getCurrentSeason(month: number): string {
    console.log(month);
    switch (month) {
      case 3:
      case 4:
      case 5:
        return 'spring';
      case 6:
      case 7:
      case 8:
        return 'summer';
      case 9:
      case 10:
      case 11:
        return 'fall';
      default:
        return 'winter';
    }
  }
}
