import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '@simulcast/material';

import { AnimeComponent } from './anime/anime.component';
import { AppComponent } from './app.component';
import { SharedModule } from '@simulcast/shared';


@NgModule({
  declarations: [
    AppComponent,
    AnimeComponent,
    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    SharedModule,
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
