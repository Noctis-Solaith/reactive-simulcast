import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { AnimeCollection } from './anime';
import { AnimeService } from './anime.service';

@Component({
  selector: 'app-anime',
  templateUrl: './anime.component.html',
  styleUrls: ['./anime.component.css']
})
export class AnimeComponent implements OnInit {
  cols: number;
  animes$: Observable<any>;

  constructor(private animeService: AnimeService) {}

  ngOnInit() {
    this.cols = 5;
    this.animes$ = this.animeService.getAnimes().pipe(
      // map((animes) => animes)
      map((animes) => Object.values(animes))
      // reduce((animes, anime) => [...animes, ...anime.flat()], [])
    );
  }
}
