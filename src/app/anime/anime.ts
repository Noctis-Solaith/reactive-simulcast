export interface Anime {
  picture: string;
  synopsis: string;
  licensor: string;
  title: string;
  link: string;
  genres: string[];
  producers: string[];
  fromType: string;
  nbEp: string;
  releaseDate: string;
  score: string;
  members: string;
}

// export interface TV extends AnimeBody {}

// export interface TVNew extends AnimeBody {}

// export interface TvContinuing extends AnimeBody {}

// export interface OVA extends AnimeBody {}

// export interface ONA extends AnimeBody {}

// export interface Movie extends AnimeBody {}

// export interface Special extends AnimeBody {}

export interface AnimeCollection {
  tv: Anime[];
  tvNew: Anime[];
  tvContinuing: Anime[];
  ova: Anime[];
  ona: Anime[];
  movies: Anime[];
  specials: Anime[];
}
